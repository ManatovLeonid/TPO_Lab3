using OpenQA.Selenium;

namespace AskFmTests.Pages
{
    public class FirstLoginSignInPage
    {
        private readonly By _logInButton = By.XPath("/html/body/main/div/a[2]");

        private IWebDriver _webDriver;

        public FirstLoginSignInPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public FirstLoginSignInPage LogIn()
        {
            _webDriver.FindElement(_logInButton).Click();
            return this;
        }
    }
}