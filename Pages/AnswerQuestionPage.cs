using OpenQA.Selenium;

namespace AskFmTests.Pages
{
    public abstract class AbstractAnswerQuestionPage : AbstractPage
    {
        public By AnswerButton;
        public By AnswerTextField;

        public abstract AbstractAnswerQuestionPage AnswerQuestion(string answer);

        protected AbstractAnswerQuestionPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
        }
    }

    public class AnswerQuestionPage : AbstractAnswerQuestionPage
    {
        public AnswerQuestionPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
            AnswerButton = By.XPath("//button[@type=\"submit\"]");
            AnswerTextField = By.XPath("//textarea[@name=\"question[answer_text]\"]");
        }

        public override AbstractAnswerQuestionPage AnswerQuestion(string answer)
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, AnswerTextField, 2f);
            WebDriver.FindElement(AnswerTextField).SendKeys(answer);
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, AnswerButton, 2f);
            WebDriver.FindElement(AnswerButton).Click();

            return this;
        }
    }
}