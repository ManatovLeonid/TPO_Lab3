using OpenQA.Selenium;

namespace AskFmTests.Pages
{
    public abstract class AbstractPage
    {
        protected IWebDriver WebDriver;
        protected string url;

        protected AbstractPage(IWebDriver webDriver, string pageUrl)
        {
            WebDriver = webDriver;
            url = pageUrl;
        }

        public T OpenPage<T>() where T : AbstractPage
        {
            OpenPage();
            return (T) this;
        }

        public void OpenPage()
        {
            WebDriver.Navigate().GoToUrl(url);
            SeleniumDelays.WaitLocate(WebDriver, url);
        }
    }
}