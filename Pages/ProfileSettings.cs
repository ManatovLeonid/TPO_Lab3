using OpenQA.Selenium;

namespace AskFmTests.Pages
{
    public abstract class AbstractProfileSettingsPage : AbstractPage
    {
        public By NameTextField;
        public By LocationTextField;
        public By BioTextField;
        public By LinkTextField;
        public By InterestsTextField;


        public abstract AbstractProfileSettingsPage ChangeName(string newName);
        public abstract AbstractProfileSettingsPage ChangeBio(string newBio);
        public abstract AbstractProfileSettingsPage ChangeLocation(string newLocation);
        public abstract AbstractProfileSettingsPage ChangeLink(string newLink);
        public abstract AbstractProfileSettingsPage ChangeInterest(string newInterest);

        protected AbstractProfileSettingsPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
        }
    }


    public class ProfileSettingsPage : AbstractProfileSettingsPage
    {
        public static readonly By SaveEditButton =
            By.XPath("//input[@type=\"submit\"][@data-target=\"#profileEditForm\"]");

        public ProfileSettingsPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
            NameTextField = By.XPath("//*[@id=\"user_name\"]");
            LocationTextField = By.XPath("//*[@id=\"user_location\"]");
            BioTextField = By.XPath("//*[@id=\"user_about_me\"]");
            LinkTextField = By.XPath("//*[@id=\"user_website\"]");
            InterestsTextField = By.XPath("//input[@data-url=\"/account/settings/hashtags\"]");
        }

        public override AbstractProfileSettingsPage ChangeName(string newName)
        {
            ChangeTextField(NameTextField, newName);
            return this;
        }

        public override AbstractProfileSettingsPage ChangeBio(string newBio)
        {
            ChangeTextField(BioTextField, newBio);
            return this;
        }

        public override AbstractProfileSettingsPage ChangeLocation(string newLocation)
        {
            ChangeTextField(LocationTextField, newLocation);
            return this;
        }

        public override AbstractProfileSettingsPage ChangeLink(string newLink)
        {
            ChangeTextField(LinkTextField, newLink);
            return this;
        }

        public override AbstractProfileSettingsPage ChangeInterest(string newInterest)
        {
            ChangeTextField(InterestsTextField, newInterest);
            return this;
        }

        private void ChangeTextField(By locator, string text)
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, locator, 2f);
            var findElement = WebDriver.FindElement(locator);
            findElement.Clear();
            findElement.SendKeys(text);
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, SaveEditButton, 2f);
            WebDriver.FindElement(SaveEditButton).Click();
        }
    }
}