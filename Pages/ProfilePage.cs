using System;
using OpenQA.Selenium;

namespace AskFmTests.Pages
{
    public abstract class AbstractProfilePage : AbstractPage
    {
        public By QuestionTextField;
        public By QuestionButton;
        public By BioText;
        public By LocationText;
        public By LinkText;
        public By InterestText;
        public By ProfileEditButton;
        public By NameText;


        public By MoreButton;
        public By DeleteButton;


        public abstract AbstractProfileSettingsPage OpenProfileSettings();
        public abstract AbstractProfilePage AskYourSelf(string question);
        public abstract AbstractProfilePage RemoveQuestion(int index);

        public abstract string GetName();
        public abstract string GetBio();
        public abstract string GetLocation();
        public abstract string GetLink();
        public abstract string GetInterests();

        public abstract int GetQuestionsCount();

        protected AbstractProfilePage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
        }
    }

    public class ProfilePage : AbstractProfilePage
    {
        public ProfilePage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
            QuestionTextField = By.XPath("//*[@id=\"question_question_text\"]");
            QuestionButton = By.XPath("//button[@data-action=\"QuestionSave\"]");
            BioText = By.XPath("(//div[@class=\"icon-bio\"])[2]");
            LocationText = By.XPath("(//div[@class=\"icon-location\"])[2]");
            LinkText = By.XPath("(//div[@class=\"icon-link\"])[2]");
            InterestText = By.XPath("(//div[@class=\"icon-interest\"])[2]");
            ProfileEditButton = By.XPath("(//a[@class=\"icon-edit\"][@href=\"/account/settings/profile\"])[2]");
            NameText = By.XPath("//*[@class=\"userName_status\"]/span[@class=\"userName\"]");


            MoreButton = By.XPath("//a[@data-action=\"PopoverOpen\"][@class=\"icon-more streamItem_option\"]");
            DeleteButton = By.XPath("//a[@data-action=\"ItemDelete\"]");
        }


        public override AbstractProfilePage AskYourSelf(string question)
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, QuestionTextField, 0.5f);
            WebDriver.FindElement(QuestionTextField).SendKeys(question);
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, QuestionButton, 0.5f);
            WebDriver.FindElement(QuestionButton).Click();

            return this;
        }

        public override AbstractProfilePage RemoveQuestion(int index)
        {
            try
            {
                SeleniumDelays.WaitElementVisibleClickable(WebDriver, MoreButton, 2f);
                WebDriver.FindElements(MoreButton)[index].Click();
                SeleniumDelays.WaitElementVisibleClickable(WebDriver, DeleteButton, 2f);
                WebDriver.FindElements(DeleteButton)[index].Click();
               
                SeleniumDelays.WaitInterval(0.5f);
                
                WebDriver.SwitchTo().Alert().Accept();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Cant get this question index");
                throw;
            }

            return this;
        }

        public override AbstractProfileSettingsPage OpenProfileSettings()
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, ProfileEditButton, 0.5f);
            WebDriver.FindElement(ProfileEditButton).Click();
            SeleniumDelays.WaitLocate(WebDriver, TestSettings.ProfileSettingsUrl);

            return new ProfileSettingsPage(WebDriver, TestSettings.ProfileSettingsUrl);
        }

        public override string GetName()
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, NameText, 3f);
            return GetTextElement(NameText);
        }

        public override string GetBio()
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, BioText, 3f);
            return GetTextElement(BioText);
        }

        public override string GetLocation()
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, LocationText, 3f);
            return GetTextElement(LocationText);
        }

        public override string GetLink()
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, LinkText, 3f);
            return GetTextElement(LinkText);
        }

        public override string GetInterests()
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, InterestText, 3f);
            return GetTextElement(InterestText);
        }

        public override int GetQuestionsCount()
        {
            // SeleniumDelays.WaitElementExist(WebDriver, MoreButton, 2f);
            return WebDriver.FindElements(MoreButton).Count;
        }


        private string GetTextElement(By locator)
        {
            return WebDriver.FindElement(locator).Text;
        }
    }
}