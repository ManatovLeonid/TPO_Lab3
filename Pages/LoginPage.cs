using NUnit.Framework;
using NUnit.Framework;
using System;
using AskFmTests.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace AskFmTests.Pages
{
    public abstract class AbstractLoginPage : AbstractPage
    {
        public abstract AbstractLoginPage Login(string login, string password);

        protected AbstractLoginPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
        }
    }

    public class LoginPage : AbstractLoginPage
    {
        private readonly By _logInTextField = By.XPath("//*[@id=\"user_name\"]");
        private readonly By _passwordTextField = By.XPath("//*[@id=\"user_password\"]");
        private readonly By _logInButton = By.XPath("//*[@id=\"sessionNewForm\"]/div[5]/input");

        public LoginPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
        }

        public override AbstractLoginPage Login(string login, string password)
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, _logInTextField, 0.5f);
            WebDriver.FindElement(_logInTextField).SendKeys(login);
            WebDriver.FindElement(_passwordTextField).SendKeys(password);

            SeleniumDelays.WaitElementVisibleClickable(WebDriver, _logInButton, 0.5f);
            WebDriver.FindElement(_logInButton).Click();

            return this;
        }
    }
}