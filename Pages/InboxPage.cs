using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AskFmTests.Pages
{
    public abstract class AbstractInboxPage : AbstractPage
    {
        public By QuestionsCountText;
        public By QuestionAnswerButton;
        public By QuestionsDeleteButton;
        public By MoreButton;
        public By AllQuestionsDeleteButton;

        public abstract int GetQuestionsCount();
        public abstract AnswerQuestionPage AnswerQuestionButton(int index);
        public abstract AbstractInboxPage DeleteQuestion(int index);
        public abstract AbstractInboxPage DeleteAllQuestions();

        protected AbstractInboxPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
        }
    }

    public class InboxPage : AbstractInboxPage
    {
        public InboxPage(IWebDriver webDriver, string pageUrl) : base(webDriver, pageUrl)
        {
            AllQuestionsDeleteButton = By.XPath("//a[@id=\"questions-delete\"]");
            QuestionsCountText = By.XPath("//span[@class=\"questionCounter question-count\"]");
            QuestionsDeleteButton = By.XPath("//a[@data-action=\"QuestionDelete\"]");
            MoreButton = By.XPath("//a[@data-action=\"PopoverOpen\"]");
            QuestionAnswerButton =
                By.XPath("//article[@class=\"item streamItem streamItem-question\"]/*/a[contains(@href,'/account/')]");
        }

        public override int GetQuestionsCount()
        {
            SeleniumDelays.WaitElementExist(WebDriver, QuestionsCountText, 2f);
            var count = int.Parse(WebDriver.FindElement(QuestionsCountText).GetAttribute("innerHTML"));
            return count;
        }

        public override AnswerQuestionPage AnswerQuestionButton(int index)
        {
            try
            {
                SeleniumDelays.WaitElementVisibleClickable(WebDriver, QuestionAnswerButton, 2f);
                WebDriver.FindElements(QuestionAnswerButton)[index].Click();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Cant get this question index");
                throw;
            }

            return new AnswerQuestionPage(WebDriver, WebDriver.Url);
        }

        public override AbstractInboxPage DeleteQuestion(int index)
        {
            try
            {
                SeleniumDelays.WaitElementVisibleClickable(WebDriver, MoreButton, 2f);
                WebDriver.FindElements(MoreButton)[index].Click();

                SeleniumDelays.WaitElementVisibleClickable(WebDriver, QuestionsDeleteButton, 2f);
                WebDriver.FindElements(QuestionsDeleteButton)[index].Click();
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Cant get this question index");
                throw;
            }

            return this;
        }

        public override AbstractInboxPage DeleteAllQuestions()
        {
            SeleniumDelays.WaitElementVisibleClickable(WebDriver, AllQuestionsDeleteButton, 2f);
            WebDriver.FindElement(AllQuestionsDeleteButton).Click();
            SeleniumDelays.WaitInterval(1f);
            WebDriver.SwitchTo().Alert().Accept();
            return this;
        }
    }
}