using System;
using System.Threading.Tasks;
using System;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;


namespace AskFmTests
{
    public static class SeleniumDelays
    {
        public static void WaitInterval(float seconds)
        {
            Task.Delay(TimeSpan.FromSeconds(seconds)).Wait();
        }

        public static void WaitElementVisibleClickable(IWebDriver webDriver, By locator, float seconds)
        {
            WaitElementVisible(webDriver, locator, seconds);
            WaitElementClickable(webDriver, locator, seconds);
        }


        public static void WaitElementVisible(IWebDriver webDriver, By locator, float seconds)
        {
            new WebDriverWait(webDriver, TimeSpan.FromSeconds(seconds)).Until(
                ExpectedConditions.ElementIsVisible(locator));
           
        }
        public static void WaitElementExist(IWebDriver webDriver, By locator, float seconds)
        {
            new WebDriverWait(webDriver, TimeSpan.FromSeconds(seconds)).Until(
                ExpectedConditions.ElementExists(locator));
           
        }


        public static void WaitElementClickable(IWebDriver webDriver, By locator, float seconds)
        {
            new WebDriverWait(webDriver, TimeSpan.FromSeconds(seconds)).Until(
                ExpectedConditions.ElementToBeClickable(locator));
        }


        public static void WaitLocate(IWebDriver webDriver, string url)
        {
            try
            {
                new WebDriverWait(webDriver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.UrlContains(url));
            }
            catch (WebDriverTimeoutException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}