using System;
using AskFmTests.Pages;
using NUnit.Framework;

namespace AskFmTests
{
    public class ProfileSettingsTest : BaseTest
    {
        [Test]
        public void ChangeNameTest()
        {
            var newName = $"Name {DateTime.Now.Hour.ToString()} : {DateTime.Now.Minute.ToString()}";

            var profileSettingsPage = OpenProfileSettingsPage()
                .ChangeName(newName);

            var profilePage = GetProfilePage();
            Assert.AreEqual(newName, profilePage.GetName());
        }

        [Test]
        public void ChangeBioTest()
        {
            var newBio = $"Bio {DateTime.Now.Hour.ToString()} : {DateTime.Now.Minute.ToString()}";

            var profileSettingsPage = OpenProfileSettingsPage()
                .ChangeBio(newBio);

            var profilePage = GetProfilePage();
            Assert.AreEqual(newBio, profilePage.GetBio());
        }


        [Test]
        public void ChangeLinkTest()
        {
            var newLink = $"Link {DateTime.Now.Hour.ToString()} : {DateTime.Now.Minute.ToString()}";

            var profileSettingsPage = OpenProfileSettingsPage()
                .ChangeLink(newLink);

            var profilePage = GetProfilePage();
            Assert.AreEqual(newLink, profilePage.GetLink());
        }

        [Test]
        public void ChangeLocationTest()
        {
            var newLocation = $"Location {DateTime.Now.Hour.ToString()} : {DateTime.Now.Minute.ToString()}";

            var profileSettingsPage = OpenProfileSettingsPage()
                .ChangeLocation(newLocation);

            var profilePage = GetProfilePage();
            Assert.AreEqual(newLocation, profilePage.GetLocation());
        }

        [Test]
        public void ChangeInterestsTest()
        {
            var newInterests = $"Interests {DateTime.Now.Hour.ToString()} {DateTime.Now.Minute.ToString()}";

            var profileSettingsPage = OpenProfileSettingsPage()
                .ChangeInterest(newInterests);

            var profilePage = GetProfilePage();
            Assert.AreEqual($"#{DateTime.Now.Minute.ToString()} #{DateTime.Now.Hour.ToString()} #interests", profilePage.GetInterests());
        }
    }
}