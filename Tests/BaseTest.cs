using NUnit.Framework;
using NUnit.Framework;
using System;
using AskFmTests.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace AskFmTests
{
    public class BaseTest
    {
        protected IWebDriver WebDriver;

        [OneTimeSetUp]
        protected void OneTimeSetUp()
        {
            // Указываем профиль в передаваемых опциях
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--user-data-dir=C:/Users/manat/AppData/Local/Google/Chrome/User Data");
            options.AddArgument("--profile-directory=/Users/manat/AppData/Local/Google/Chrome/User Data/Profile 1");

            WebDriver = new ChromeDriver(options);
            // WebDriver = new ChromeDriver();
        }

        [SetUp]
        protected void Setup()
        {
            WebDriver.Manage().Window.Maximize();
            WebDriver.Navigate().GoToUrl(TestSettings.MainUrl);
            SeleniumDelays.WaitLocate(WebDriver, TestSettings.MainUrl);
        }

        [OneTimeTearDown]
        protected void TearDown()
        {
            // WebDriver.Quit();
        }

        #region ProfileSettingsPage

        protected AbstractProfileSettingsPage OpenProfileSettingsPage()
        {
            return GetProfileSettingsPage()
                .OpenPage<ProfileSettingsPage>();
        }

        protected AbstractProfileSettingsPage GetProfileSettingsPage()
        {
            return new ProfileSettingsPage(WebDriver, TestSettings.ProfileSettingsUrl);
        }

        #endregion

        #region ProfilePage

        protected AbstractProfilePage OpenProfilePage()
        {
            return GetProfilePage()
                .OpenPage<ProfilePage>();
        }

        protected AbstractProfilePage GetProfilePage()
        {
            return new ProfilePage(WebDriver, TestSettings.ProfileUrl);
        }

        #endregion

        #region InboxPage

        protected AbstractInboxPage OpenInbox()
        {
            return GetInbox().OpenPage<AbstractInboxPage>();
        }

        protected AbstractInboxPage GetInbox()
        {
            return new InboxPage(WebDriver, TestSettings.InboxUrl);
        }

        #endregion
    }
}