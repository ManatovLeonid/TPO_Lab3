using System;
using AskFmTests.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.DevTools;

namespace AskFmTests
{
    public class ProfileTest : BaseTest
    {
        [Test]
        public void AskYourselfTest()
        {
            OpenProfilePage().AskYourSelf(
                $"QuestionTest {DateTime.Now.Hour.ToString()} : {DateTime.Now.Minute.ToString()} ?");

            SeleniumDelays.WaitInterval(1f);


            // Проверка появления панели "Готово"
            var gotovoStyle = WebDriver.FindElement(By.XPath("//*[@id=\"topMenu\"]/div[3]")).GetAttribute("style");
            Console.WriteLine(gotovoStyle);
            Assert.AreNotEqual("display: none;", gotovoStyle);
        }

        [Test]
        public void ProfileSettingsOpenTest()
        {
            var profileSettingsPage = OpenProfilePage().OpenProfileSettings();

            Assert.AreEqual(TestSettings.ProfileSettingsUrl, WebDriver.Url);
        }

        [Test]
        public void DeleteQuestionTest()
        {
            var profilePage = OpenProfilePage();
            var before = profilePage.GetQuestionsCount();
            
            if (before == 0) Assert.Ignore("No questions");
            
            profilePage.RemoveQuestion(0);

            SeleniumDelays.WaitInterval(1f);

            Assert.AreEqual(before - 1, profilePage.GetQuestionsCount());
        }
    }
}