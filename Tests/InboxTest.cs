using System;
using AskFmTests.Pages;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AskFmTests
{
    public class InboxTest : BaseTest
    {
        [Test]
        public void AnswerQuestionTest()
        {
            var inBox = OpenInbox();
            var beforeCount = inBox.GetQuestionsCount();
            
            if (beforeCount == 0) Assert.Ignore("No questions");
            
            var answerQuestionPage = inBox.AnswerQuestionButton(0)
                .AnswerQuestion($"Answer {DateTime.Now.Hour.ToString()} : {DateTime.Now.Minute.ToString()}");


            SeleniumDelays.WaitInterval(1f);
            Assert.AreEqual(beforeCount - 1, inBox.GetQuestionsCount());
        }

        [Test]
        public void DeleteQuestionTest()
        {
            var inboxPage = OpenInbox();
            var beforeCount = inboxPage.GetQuestionsCount();
            if (beforeCount == 0) Assert.Ignore("No questions");

            inboxPage.DeleteQuestion(0);
            SeleniumDelays.WaitInterval(1f);
            Assert.AreEqual(beforeCount - 1, inboxPage.GetQuestionsCount());
        }


        [Test]
        public void DeleteAllQuestionsTest()
        {
            var inboxPage = OpenInbox();
            var beforeCount = inboxPage.GetQuestionsCount();
            if (beforeCount == 0) Assert.Ignore("No questions");

            inboxPage.DeleteAllQuestions();
            SeleniumDelays.WaitInterval(1f);
            Assert.AreEqual(0, inboxPage.GetQuestionsCount());
        }
    }
}