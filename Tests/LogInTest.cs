using NUnit.Framework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using AskFmTests.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace AskFmTests
{
    public class LogInTest : BaseTest
    {
        [Test]
        public void Test1()
        {
            //WebDriver.Manage().Cookies.DeleteAllCookies();
            var firstPage = new FirstLoginSignInPage(WebDriver).LogIn();
            SeleniumDelays.WaitLocate(WebDriver, TestSettings.LoginUrl);

            var loginPage = new LoginPage(WebDriver, TestSettings.LoginUrl);

            loginPage.OpenPage<LoginPage>().Login(TestSettings.Login, TestSettings.Password);


            Assert.Pass();
        }
    }
}